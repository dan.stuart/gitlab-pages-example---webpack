import Vue from 'vue';
import vueCustomElement from 'vue-custom-element';
import 'document-register-element/build/document-register-element';

import App from './App.vue';
import router from './router';
import store from './store/index';

Vue.use(vueCustomElement);
App.store = store;
App.router = router;

Vue.customElement('example-vue-widget', App);
