const BrowserSyncPlugin = require('browser-sync-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const ExtraWatchWebpackPlugin = require('extra-watch-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HTMLWebpackPlugin = require('html-webpack-plugin');
const HtmlBeautifyPlugin = require('html-beautify-webpack-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const StyleLintPlugin = require('stylelint-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const path = require('path');
const fs = require('fs');

// Our function that generates our html plugins
function generateHtmlTemplates(templateDir) {
  // Read files in template directory
  const templateFiles = fs.readdirSync(path.resolve(__dirname, templateDir));
  return templateFiles.map(item => {
    // Split names and extension
    const parts = item.split('.');
    const name = parts[0];
    const extension = parts[1];
    // Create new HtmlWebpackPlugin with options
    return new HTMLWebpackPlugin({
      filename: `${name}.html`,
      template: path.resolve(__dirname, `${templateDir}/${name}.${extension}`),
    });
  });
}

// Call our function on our views directory.
const htmlTemplates = generateHtmlTemplates('./templates/views');

module.exports = env => {
  const devMode = !env || !env.production;

  return {
    mode: devMode ? 'development' : 'production',
    entry: {
      main: './src/index.js',
      vendor: './src/vendor.js',
    },
    output: {
      path: path.resolve(__dirname, 'public'),
      filename: 'assets/js/[name].js',
      library: 'MainModule',
    },
    module: {
      rules: [
        {
          test: /\.(sa|sc|c)ss$/,
          use: [
            MiniCssExtractPlugin.loader,
            'css-loader',
            'postcss-loader',
            'sass-loader',
          ],
        },
        {
          enforce: 'pre',
          test: /\.(js|vue)$/,
          exclude: /node_modules/,
          loader: 'eslint-loader',
        },
        {
          test: /\.js$/,
          loader: 'babel-loader',
          query: {
            presets: ['es2015'],
          },
        },
        {
          test: /\.(njk|nunjucks|html|tpl|tmpl)$/,
          use: [
            {
              loader: 'nunjucks-isomorphic-loader',
              query: {
                root: [path.resolve(__dirname, 'templates')],
              },
            },
          ],
        },
        {
          test: /\.vue$/,
          loader: 'vue-loader',
          options: {
            loaders: {
              css: [
                'vue-style-loader',
                {
                  loader: 'sass-loader',
                },
              ],
              js: ['babel-loader'],
            },
            cacheBusting: true,
          },
        },
      ],
    },
    stats: {
      colors: true,
    },
    devtool: devMode ? false : 'source-map',
    plugins: [
      new MiniCssExtractPlugin({
        filename: 'assets/css/[name].css',
      }),
      new StyleLintPlugin(),
      new BrowserSyncPlugin({
        host: 'localhost',
        port: 3000,
        server: { baseDir: ['public'] },
      }),
      new ExtraWatchWebpackPlugin({
        dirs: ['templates'],
      }),
      new CopyWebpackPlugin([{ from: 'assets/**/*', to: '.' }]),
      new CleanWebpackPlugin(['public']),
      new VueLoaderPlugin(),
    ]
      // We join our htmlPlugin array to the end
      // of our webpack plugins array.
      .concat(htmlTemplates)
      .concat([
        new HtmlBeautifyPlugin({
          config: {
            html: {
              indent_size: 2,
              indent_with_tabs: false,
              preserve_newlines: false,
              unformatted: ['p', 'i', 'b', 'span'],
            },
          },
          replace: [' type="text/javascript"'],
        }),
      ]),
    optimization: {
      minimizer: [
        new UglifyJsPlugin({
          sourceMap: true,
          parallel: true,
        }),
        new OptimizeCSSAssetsPlugin({
          cssProcessorOptions: {
            map: {
              inline: false,
            },
          },
        }),
      ],
    },
  };
};
