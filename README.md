[![pipeline status](https://gitlab.com/dan.stuart/gitlab-pages-example---webpack/badges/master/pipeline.svg)](https://gitlab.com/dan.stuart/gitlab-pages-example---webpack/commits/master)



# A complete scaffolding for Webpack + Nunjucks + Browsersync + Sass + Babel + Eslint + Stylelint + Vue Widgets

## Install Dependancies

`npm install`

## For Development

`npm run watch`

`npm run watch:build`

## For Production

`npm run build`

`npm run build:prod`

## Vue

https://itnext.io/vuidget-how-to-create-an-embeddable-vue-js-widget-with-vue-custom-element-674bdcb96b97

Inspiration was taken from this post about how to make mini Vue components and add them on a webpage.

## ToDo

* SVG Sprite

<table>
  <tbody>
    <tr>
      <td align="center">
        <a href="https://gitlab.com/dan.stuart">
          <img width="150" height="150" src="https://secure.gravatar.com/avatar/1790c21045b662c0465c7dd533814f52?s=800&d=identicon">
          <br>
          Dan Stuart
        </a>
      </td>
    </tr>
  <tbody>
</table>